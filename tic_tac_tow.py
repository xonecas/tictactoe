#!/usr/bin/env python3

import random


class TicTacToe():

    class InvalidPlay(Exception):
        """Error for when the Player chooses an invalid move"""
        pass

    AI = 1
    PLAYER = 2
    SYMBOLS = {0: '-', 1: 'O', 2: 'X', }

    def __init__(self):
        self.board = [[0] * 3 for i in [0] * 3]
        # Simple heads or tails, over 5.0 goes the player
        self.current_player = (
            self.PLAYER if random.random() >= 0.5 else self.AI)
        self.total_moves = 0

    def printBoard(self):
        symbols_to_print = ''
        for i, n in enumerate(self.board):
            for j, m in enumerate(n):
                symbols_to_print = symbols_to_print + self.SYMBOLS[m]
                if j < 2:
                    symbols_to_print = symbols_to_print + '|'
                else:
                    symbols_to_print = symbols_to_print + '\n'
        print('\n' + symbols_to_print + '\n')

    def checkForRoom(self):
        foundRoom = False

        for i, _ in enumerate(self.board):
            for j, m in enumerate(self.board[i]):
                if m == 0:
                    foundRoom = True
                    break
            if foundRoom:
                break

        return foundRoom

    def checkForWinner(self):
        board = self.board
        # Check horizontals, then verticals, then diag:
        if (
            board[0][0] != 0 and board[0][0] == board[0][1] == board[0][2] or
            board[1][0] != 0 and board[1][0] == board[1][1] == board[1][2] or
            board[2][0] != 0 and board[2][0] == board[2][1] == board[2][2] or
            board[0][0] != 0 and board[0][0] == board[1][0] == board[2][0] or
            board[0][1] != 0 and board[0][1] == board[1][1] == board[2][1] or
            board[0][2] != 0 and board[0][2] == board[1][2] == board[2][2] or
            board[0][0] != 0 and board[0][0] == board[1][1] == board[2][2] or
            board[2][0] != 0 and board[2][0] == board[1][1] == board[0][2]
        ):
            return True
        return False

    def minimax_calculation(self, depth=2):
        x = -1
        y = -1
        # -1 for loss, 0 ties, 1 wins
        score = -2
        best_score = -1  # Start loosing

        if self.checkForRoom():
            for i, _ in enumerate(self.board):
                for j, m in enumerate(self.board[i]):
                    if m == 0:
                        self.board[i][j] = self.current_player

                        if self.current_player == self.AI:
                            # Maximing value
                            self.current_player = self.PLAYER
                            score, x, y = self.minimax_calculation(depth - 1)
                            if best_score < score:
                                best_score = score
                                x = i
                                y = j
                        else:
                            # Minimizing value
                            self.current_player = self.AI
                            score, x, y = self.minimax_calculation(depth - 1)
                            if best_score > score:
                                best_score = score
                                x = i
                                y = j
                        self.board[i][j] = m
            if self.checkForWinner():
                return (1 if self.current_player == self.AI else -1), x, y
        else:
            return 0, x, y

        return best_score, x, y

    def loop(self):
        game_running = True

        while(game_running):
            self.printBoard()

            # game needs at least 5 moves to be a winner
            if self.total_moves >= 5:
                game_won = self.checkForWinner()
                if game_won:
                    winner = ('You' if self.PLAYER == self.current_player
                              else 'The AI')
                    print('\nGameOver')
                    print(f"{winner} won in {self.total_moves} moves\n")
                    game_running = False
                    break

            if not self.checkForRoom():
                print('\nGameOver, You tied')
                game_running = False
                break

            self.current_player = (self.PLAYER if
                                   self.current_player == self.AI else self.AI)
            try:
                if self.current_player == self.PLAYER:
                    player_coords_input = input(
                        '\nEnter a row and column, separeted by a comma\n\n')
                    i, j = [int(s) - 1 for s in player_coords_input.split(',')]
                    allowed_indexes = (0, 1, 2)

                    if (i in allowed_indexes and j in allowed_indexes and
                            self.board[i][j] == 0):
                        self.board[i][j] = self.PLAYER
                    else:
                        raise self.InvalidPlay()
                else:
                    print('AI is making their move...')
                    _, i, j = self.minimax_calculation()
                    print(f'Done: {i}, {j}')
                    self.current_player = self.AI
                    self.board[i][j] = self.AI
            except self.InvalidPlay:
                print('\nInvalid coordenates, please choose a valid spot\n\n')
                self.current_player = self.AI
                self.loop()
                break

            self.total_moves = self.total_moves + 1


if __name__ == '__main__':
    game = TicTacToe()
    game.loop()
